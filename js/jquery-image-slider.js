"use strict";

var slideIndex = 1;
slides(slideIndex);

function plusSlides(n) {
    slides(slideIndex += n);
}

function currentSlide(n) {
    slides(slideIndex = n);
}

function slides(n) {
    var i;
    var slides = document.getElementsByClassName("slides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) {slideIndex = 1}
    if (n < 1) {slideIndex = slides.length}
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
        dots[i].className = dots[i].className.replace(" active", "");
    }
    slides[slideIndex-1].style.display = "block";
    dots[slideIndex-1].className += " active";
}

$('.title').click(function () {
    $('.hide-toggle').toggle(2500)
});

// $('.next').click(function () {
//     $('#jjr').fadeIn(1500)
// })
